<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tiago Coito Portfolio Website</title>
        <link rel="icon" href="img/logo.png" type="image/icon type">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />

        <!-- Update these with your own fonts -->

        <link rel="stylesheet" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Nunito:200" rel="stylesheet" />
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    </head>
    <body data-aos-easing="ease-out-back" data-aos-duration="1000" data-aos-delay="1">
        <header>
            <button class="nav-toggle" aria-label="toggle navigation">
                <span class="hamburger"></span>
            </button>
            <nav class="nav">
                <ul class="nav__list">
                    <li class="nav__item"><a href="#home" class="nav__link">Home</a></li>
                    <!--<li class="nav__item"><a href="#services" class="nav__link">My Services</a></li>-->
                    <li class="nav__item"><a href="#about" class="nav__link">About me</a></li>
                    <!--<li class="nav__item"><a href="#work" class="nav__link">My Work</a></li>-->
                    <li class="nav__item"><a href="#experience" class="nav__link">My Experience</a></li>
                </ul>
            </nav>
        </header>

        <!-- Landing-text -->
        <section class="landing-text">
            <h1>TIAGO COITO</h1>
            <h6>Software Developer</h6>
        </section>

        <!-- About me -->
        <section class="about-me" id="about">

            <h2 class="section__title section__title--about">Who I am</h2>

            <div class="about-me__body">
                <p>Passionate about technology as long as I can remember for that reason I' am graduated in Computer Engineering.</p>
                <p>My professional and academic experience allowed me to work with various technologies,
                   adapting easily to them, developing skills such as teamwork, communication, planning and organization.</p>

                <p>I believe that knowledge is essential, taking into account the rapid technological evolution,
                   so I keep learning to improve myself.</p>
            </div>
            <div class="about-me__img"></div>

            <div class="about-me__social">
                <ul class="icon-list">
                    <li class="icon-item">
                        <a class="icon-link" href="https://www.facebook.com/tiago.coito">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://www.instagram.com/tiag0coito">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://twitter.com/TiagoCoito">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://www.youtube.com/channel/UCw-9Qnf5Iv9I-wiONio-ibg">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://www.linkedin.com/in/tiago-coito">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://github.com/tiag0coito">
                            <i class="fab fa-github"></i>
                        </a>
                    </li>
                    <li class="icon-item">
                        <a class="icon-link" href="https://gitlab.com/tiag0coito">
                            <i class="fab fa-gitlab"></i>
                        </a>
                    </li>
                 </ul>
            </div>

         </section>

        <!-- My services -->
        <!--<section class="my-services" id="services">
            <h2 class="section__title section__title--services">What I do</h2>
            <div class="services">
                <div class="service aos-init aos-animate" data-aos="fade-up">
                    <h3>Design + Development</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>

                <div class="service aos-init aos-animate" data-aos="fade-up">
                    <h3>E-Commerce</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>

                <div class="service aos-init aos-animate" data-aos="fade-up">
                    <h3>WordPress</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>

            <a href="#work" class="btn">My Work</a>
        </section>-->




        <!-- My Work -->
        <!-- <section class="my-work" id="work">
            <h2 class="section__title section__title--work">My work</h2>
            <p class="section__subtitle section__subtitle--work">A selection of my range of work</p>

            <div class="portfolio">

                <a href="portfolio-item.html" class="portfolio__item">
                    <img src="img/image_1.jpg" alt="" class="portfolio__img">
                </a>


                <a href="portfolio-item.html" class="portfolio__item">
                    <img src="img/image_1.jpg" alt="" class="portfolio__img">
                </a>


                <a href="portfolio-item.html" class="portfolio__item">
                    <img src="img/image_2.jpg" alt="" class="portfolio__img">
                </a>


                <a href="portfolio-item.html" class="portfolio__item">
                    <img src="img/image_3.jpg" alt="" class="portfolio__img">
                </a>
            </div>
        </section>-->

        <div class="experience" id="experience">
                <div class="section-heading">
                    <h2 class="section__title section__title--about">Work Experience</h2>
                    <p class="section__subtitle ">Past and Current Jobs</p>
                </div>
                <div class="timeline">
                    <ul>
                        <li class="date" data-date="Jan 2019 - Present">
                            <h1>La Redoute</h1>
                            <p>
                                Redoute is a French fashion and furniture company that currently operates in 26 countries and has more than 10 million active customers.</p>
                                <br>
                                <p>I am part of Application Support team that provide support for the internal IT system used by company's subsidiaries (PT, ES, FR, CH, BE, IT, RU, UK and .COM).
                                From solving technical problems or developing new features, including batch and web applications.</p>
                                <br>
                                <p></p>Analysis and resolution of incidents reported by company's subsidiaries in order to solve problems quickly to keep the normal system.</p>
                                <br>
                                <p>Technologies used:
                                Programming languages: COBOL, Java, JavaScript, SQL, C #
                                Frameworks: Spring Boot, Spring MVC, .NET, liquibase
                                Database: DB2, Orable and PostgresSQL.</p>
                        </li>

                        <li class="date" data-date="Jan 2017 - dec 2018">
                            <h1>Prime IT</h1>
                            <p>
                                Prime IT is a consulting company specialized in the IT and Telecommunications sector.</p>
                                <br>
                                <p>As a consultant, I worked as a software developer at Santander developing API RESTful applications with
                                    technologies such as Spring Boot, Spring MVC, Spring Batch, Hibernate and Oracle.</p>
                                <br>
                        </li>

                        <li class="date" data-date="Jun 2011 - ago 2011">
                            <h1>Omnilog</h1>
                            <p>
                                Omnilog is dedicated to software development, developing solutions using the latest technologies such as rfid, cloud computing or mobile.</p>
                                <br>
                                <p> Development Visual Basic .NET application</p>
                                <br>
                        </li>
                    </ul>
                </div>
            </div>

        <!-- Footer -->
        <footer class="footer">
            <!-- replace with your own email address -->
            <a href="mailto:tiagocoito92@gmail.com" class="footer__link">tiago.coito92@gmail.com</a>
            <ul class="social-list">
                <li class="social-list__item">
                    <a class="social-list__link" href="https://www.facebook.com/tiago.coito">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://www.instagram.com/tiag0coito">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://twitter.com/TiagoCoito">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://www.youtube.com/channel/UCw-9Qnf5Iv9I-wiONio-ibg">
                        <i class="fab fa-youtube"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://www.linkedin.com/in/tiago-coito">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://github.com/tiag0coito">
                        <i class="fab fa-github"></i>
                    </a>
                </li>
                <li class="social-list__item">
                    <a class="social-list__link" href="https://gitlab.com/tiag0coito">
                        <i class="fab fa-gitlab"></i>
                    </a>
                </li>
            </ul>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="js/index.js"></script>

    </body>
</html>
